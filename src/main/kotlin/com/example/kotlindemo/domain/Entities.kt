package com.example.kotlindemo.domain

import com.example.kotlindemo.extension.toSlug
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

//In Kotlin it is not unusual to group concise class declarations in the same file.

@Entity
//declare at the same time the properties and the constructor parameters.
class Article(
        var title: String,
        var headline: String,
        var content: String,
        @ManyToOne var author: User,
        var slug: String = title.toSlug(), //Extension we created
        var addedAt: LocalDateTime = LocalDateTime.now(),
        @Id @GeneratedValue var id: Long? = null)

@Entity
class User(
        var login: String,
        var firstname: String,
        var lastname: String,
        @Id @GeneratedValue var id: Long? = null)