package com.example.kotlindemo

import com.example.kotlindemo.domain.Article
import com.example.kotlindemo.domain.User
import com.example.kotlindemo.repository.ArticleRepository
import com.example.kotlindemo.repository.UserRepository
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class KotlinDemoConfiguration {

    @Bean
    fun databaseInitializer(userRepository: UserRepository, articleRepository: ArticleRepository) = ApplicationRunner {
        val smaldini = userRepository.save(User("smaldini", "Stéphane", "Maldini"))
        //Named parameters to make the code more readable.
        articleRepository.save(Article(
                title = "Reactor Bismuth is out",
                headline = "Lorem ipsum",
                content = "dolor sit amet",
                author = smaldini
        ))
        articleRepository.save(Article(
                author = smaldini,
                title = "Reactor Aluminium has landed",
                content = "dolor sit amet",
                headline = "Lorem ipsum"
        ))
    }
}